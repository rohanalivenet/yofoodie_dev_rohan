import { BrowserModule } from "@angular/platform-browser";
import { AngularMaterialModule } from "../material.module";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { AutocompleteLibModule } from "angular-ng-autocomplete";


import { FlightRoutingModule } from "./flight-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatMomentDateModule, MomentDateAdapter,MAT_MOMENT_DATE_ADAPTER_OPTIONS } from "@angular/material-moment-adapter";

import { SwiperModule } from "ngx-swiper-wrapper";
import { SWIPER_CONFIG } from "ngx-swiper-wrapper";
import { SwiperConfigInterface } from "ngx-swiper-wrapper";

import { HomeComponent } from "../home/home.component";
import { FlightComponent } from "./flight/flight.component";
import { FlightHomeComponent } from "./flight-home/flight-home.component";
import { FlightSearchComponent } from "./flight-search/flight-search.component";
import { FlightTravellerComponent } from "./flight-traveller/flight-traveller.component";
import { FlightHomeSearchComponent } from './flight-home-search/flight-home-search.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: "horizontal",
  slidesPerView: "auto"
};

@NgModule({
  declarations: [
    FlightComponent,
    FlightHomeComponent,
    FlightSearchComponent,
    FlightTravellerComponent,
    HomeComponent,
    FlightHomeSearchComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    AngularMaterialModule,
    FlightRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SwiperModule,
    MatDatepickerModule, 
    MatNativeDateModule,
    MatMomentDateModule,
    AutocompleteLibModule
  ],
  providers: [
    { provide: SWIPER_CONFIG,useValue: DEFAULT_SWIPER_CONFIG },
    {provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: {strict: true}}
  ]
})
export class FlightModule {}
