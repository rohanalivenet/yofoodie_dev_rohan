import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FlightsearchService {
  Flight_Name:string="";
  ReturnFlight_Name:string="";
  Flight_options: any[] = [];
  Flight_details:any[]=[];
  Flight_Stop:string="";
  ReturnFlight_Stop:string="";
  Flight_Stoppage_arrival:string="";
  ReturnFlight_Stoppage_arrival:string="";
  Flight_Stoppage_departure:string="";
  ReturnFlight_Stoppage_departure:string="";

  constructor() { }
  
  public searchdatacal(data){
    this.Flight_options = [];
    this.Flight_details=[];
    let promise = new Promise((resolve, reject) => {
      var totalresult = data["flightsearch"][0]["data"].length;
    for (var i = 0; i < data["flightsearch"][0]["data"].length; i++) {
      
      //Common details
      var bookableseats = data["flightsearch"][0]["data"][i]['numberOfBookableSeats'];
      var price = data["flightsearch"][0]["data"][i]['price'];
      var currency =
      data["flightsearch"][0]["data"][i]["travelerPricings"][0]["price"][
        "currency"
      ];
    var totalamount =
      data["flightsearch"][0]["data"][i]["travelerPricings"][0]["price"][
        "total"
      ];
      var Flightduration =
      data["flightsearch"][0]["data"][i]["itineraries"][0]["duration"].slice(2);
    var Flight_Details_Segment = [{segments:data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"]}];
    var Flight_Details_travelerPricings = [{travelerPricings:data["flightsearch"][0]["data"][i]["travelerPricings"]}];
      
      //Ditails depending on the search on the trip type
      //if trip is one way
      var triptype = data["flightsearch"][0]["data"][i]["itineraries"].length;
      if(triptype==1){
      var carriercode =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
          "carrierCode"
        ];
      var Carriernumber = data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
          "number"];
      
      var carriers: string =
        data["flightsearch"][0]["dictionaries"]["carriers"];
      Object.keys(carriers).forEach(key=>{
        if(key===carriercode)
        {
          this.Flight_Name= carriers[key];
        }
        
      });

      var departureFrom =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
          "departure"
        ]["iataCode"];
      var departureTerminal =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
          "departure"
        ]["terminal"];
      var departureAt =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
          "departure"
        ]["at"].split('T');
    var departureDate = departureAt[0];
    var departureTime = departureAt[1];

    
    var stops =data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"].length;
    var Flight_Details_Stop =[{stops:stops-1}];
    
    if(stops==1){
      var arrivalplace =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
          "arrival"
        ]["iataCode"];
      var arrivalterminal =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
          "arrival"
        ]["terminal"];
      var arrivalat =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
          "arrival"
        ]["at"].split('T');
      var arrivalDate = arrivalat[0];
      var arrivalTime = arrivalat[1]; 
      this.Flight_Stop = 'Non Stop';
      this.Flight_Stoppage_arrival ="";
      this.Flight_Stoppage_departure="";
      //this.Flight_Stoppage=[departureFrom,arrivalplace];
    }else{
      if(stops==2){
        this.Flight_Stoppage_departure = data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][1]["departure"]["iataCode"];
        this.Flight_Stoppage_arrival="";
        var arrivalplace =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][stops-1][
          "arrival"
        ]["iataCode"];
      var arrivalterminal =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][stops-1][
          "arrival"
        ]["terminal"];
      var arrivalat =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][stops-1][
          "arrival"
        ]["at"].split('T');
      var arrivalDate = arrivalat[0];
      var arrivalTime = arrivalat[1]; 
      this.Flight_Stop =stops-1+' Stop';

      }else{
      for (var j=1;j< stops-1;j++){
        this.Flight_Stoppage_arrival=data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][j]["arrival"]["iataCode"];
        this.Flight_Stoppage_departure = data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][j]["departure"]["iataCode"];
      } 

      var arrivalplace =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][stops-1][
          "arrival"
        ]["iataCode"];
      var arrivalterminal =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][stops-1][
          "arrival"
        ]["terminal"];
      var arrivalat =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][stops-1][
          "arrival"
        ]["at"].split('T');
      var arrivalDate = arrivalat[0];
      var arrivalTime = arrivalat[1]; 
      this.Flight_Stop =stops-1+' Stop';
    }
  } 

      var adult = 0;
      var child =0;
      var infant = 0;
      for(var j=0;j<=data["flightsearch"][0]["data"][i]["travelerPricings"].length-1; j++){
        if(data["flightsearch"][0]["data"][i]["travelerPricings"][j]['travelerType']=='ADULT'){
          adult++;  
          var TotalAmount = data["flightsearch"][0]["data"][i]["travelerPricings"][j]['price']['total'];
          var BaseAmount = data["flightsearch"][0]["data"][i]["travelerPricings"][j]['price']['base'];
          var AdultTotal = [{'AdultNumber':adult,'TotalAmount':TotalAmount,'BaseAmount':BaseAmount}];
        }
        if(data["flightsearch"][0]["data"][i]["travelerPricings"][j]['travelerType']=='CHILD'){
          child++;
          var TotalAmount = data["flightsearch"][0]["data"][i]["travelerPricings"][j]['price']['total'];
          var BaseAmount = data["flightsearch"][0]["data"][i]["travelerPricings"][j]['price']['base'];  
          var ChildTotal = [{'ChildNumber':child,'TotalAmount':TotalAmount,'BaseAmount':BaseAmount}];
        }
        if(data["flightsearch"][0]["data"][i]["travelerPricings"][j]['travelerType']=='INFANT'){
          infant++;
          var TotalAmount = data["flightsearch"][0]["data"][i]["travelerPricings"][j]['price']['total'];
          var BaseAmount = data["flightsearch"][0]["data"][i]["travelerPricings"][j]['price']['base']; 
          var InfantTotal = [{'InfantNumber':infant,'TotalAmount':TotalAmount,'BaseAmount':BaseAmount}];
        }
      }  
    }




    //if its a round trip
    else if (triptype==2){

      var OnwardFlightStops = data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"].length;
      var ReturnFlightStops = data["flightsearch"][0]["data"][i]["itineraries"][1]["segments"].length;

    //Departure for Round Trip
     //Round Trip Departure
      var departureFrom =
      data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
      "departure"
      ]["iataCode"];
      var departureTerminal =
      data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
      "departure"
      ]["terminal"];
      var departureAt =
      data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
      "departure"
      ]["at"].split('T');
      var departureDate = departureAt[0];
      var departureTime = departureAt[1];

      var carriercode =
      data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
      "carrierCode"
      ];
      var Carriernumber = data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
      "number"];

      var carriers: string =
      data["flightsearch"][0]["dictionaries"]["carriers"];
      Object.keys(carriers).forEach(key=>{
      if(key===carriercode)
      {
      this.Flight_Name= carriers[key];
      }

      });
      
    //Round Trip Return Departure
      var ReturnDepartureFrom =
      data["flightsearch"][0]["data"][i]["itineraries"][1]["segments"][0][
      "departure"
      ]["iataCode"];
      var ReturnDepartureTerminal =
      data["flightsearch"][0]["data"][i]["itineraries"][1]["segments"][0][
      "departure"
      ]["terminal"];
      var ReturnDepartureAt =
      data["flightsearch"][0]["data"][i]["itineraries"][1]["segments"][0][
      "departure"
      ]["at"].split('T');
      var ReturnDepartureDate = ReturnDepartureAt[0];
      var ReturnDepartureTime = ReturnDepartureAt[1];

      var Returncarriercode =
      data["flightsearch"][0]["data"][i]["itineraries"][1]["segments"][0][
      "carrierCode"
      ];
      var ReturnCarriernumber = data["flightsearch"][0]["data"][i]["itineraries"][1]["segments"][0][
      "number"];

      var carriers: string =
      data["flightsearch"][0]["dictionaries"]["carriers"];
      Object.keys(carriers).forEach(key=>{
      if(key===Returncarriercode)
      {
      this.ReturnFlight_Name= carriers[key];
      }

      });

    //Arrival For Round Trip
    if(OnwardFlightStops ==1 ){
    var arrivalplace =
    data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
      "arrival"
    ]["iataCode"];
  var arrivalterminal =
    data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
      "arrival"
    ]["terminal"];
  var arrivalat =
    data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][0][
      "arrival"
    ]["at"].split('T');
  var arrivalDate = arrivalat[0];
  var arrivalTime = arrivalat[1];
  this.Flight_Stop = 'Non Stop';
  this.Flight_Stoppage_arrival ="";
  this.Flight_Stoppage_departure="";
  }else if(OnwardFlightStops ==2){

    this.Flight_Stoppage_departure = data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][1]["departure"]["iataCode"];
        this.Flight_Stoppage_arrival="";
        var arrivalplace =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][OnwardFlightStops-1][
          "arrival"
        ]["iataCode"];
      var arrivalterminal =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][OnwardFlightStops-1][
          "arrival"
        ]["terminal"];
      var arrivalat =
        data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][OnwardFlightStops-1][
          "arrival"
        ]["at"].split('T');
      var arrivalDate = arrivalat[0];
      var arrivalTime = arrivalat[1]; 
      this.Flight_Stop =OnwardFlightStops-1+' Stop';
  }else{
    for(var j=1;j<OnwardFlightStops;j++){

      this.Flight_Stoppage_arrival=data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][j]["arrival"]["iataCode"];
      this.Flight_Stoppage_departure = data["flightsearch"][0]["data"][i]["itineraries"][0]["segments"][j]["departure"]["iataCode"];

    }
  }

  
  if(ReturnFlightStops ==1 ){
     //Round Trip Arrival
     var ReturnArrivalplace =
     data["flightsearch"][0]["data"][i]["itineraries"][1]["segments"][0][
       "arrival"
     ]["iataCode"];
   var ReturnArrivalterminal =
     data["flightsearch"][0]["data"][i]["itineraries"][1]["segments"][0][
       "arrival"
     ]["terminal"];
   var ReturnArrivalat =
     data["flightsearch"][0]["data"][i]["itineraries"][1]["segments"][0][
       "arrival"
     ]["at"].split('T');
   var ReturnArrivalDate = ReturnArrivalat[0];
   var ReturnArrivalTime = ReturnArrivalat[1]; 
   this.ReturnFlight_Stop = 'Non Stop';
  this.ReturnFlight_Stoppage_arrival ="";
  this.ReturnFlight_Stoppage_departure="";
   }else if(ReturnFlightStops ==2){

    this.ReturnFlight_Stoppage_departure = data["flightsearch"][0]["data"][i]["itineraries"][1]["segments"][1]["departure"]["iataCode"];
        this.ReturnFlight_Stoppage_arrival="";
        var ReturnArrivalplace =
        data["flightsearch"][0]["data"][i]["itineraries"][1]["segments"][ReturnFlightStops-1][
          "arrival"
        ]["iataCode"];
      var ReturnArrivalterminal =
        data["flightsearch"][0]["data"][i]["itineraries"][1]["segments"][ReturnFlightStops-1][
          "arrival"
        ]["terminal"];
      var ReturnArrivalat =
        data["flightsearch"][0]["data"][i]["itineraries"][1]["segments"][ReturnFlightStops-1][
          "arrival"
        ]["at"].split('T');
      var ReturnArrivalDate = ReturnArrivalat[0];
      var ReturnArrivalTime = ReturnArrivalat[1]; 
      this.ReturnFlight_Stop =ReturnFlightStops-1+' Stop';
   }else{
    for(var j=1;j<ReturnFlightStops;j++){

      this.ReturnFlight_Stoppage_arrival=data["flightsearch"][0]["data"][i]["itineraries"][1]["segments"][j]["arrival"]["iataCode"];
      this.ReturnFlight_Stoppage_departure = data["flightsearch"][0]["data"][i]["itineraries"][1]["segments"][j]["departure"]["iataCode"];
    }
   }
    }else{

    }
       
     
      
      this.Flight_options.push({
        FlightType:triptype,
        bookableseats:bookableseats,
        Flightduration: Flightduration,
        Flightcode:carriercode,
        FlightNumber: Carriernumber,
        ReturnFlightcode:Returncarriercode,
        ReturnFlightNumber:ReturnCarriernumber,
        ReturnFlightname:this.ReturnFlight_Name,
        departureFrom: departureFrom,
        departureTerminal: departureTerminal,
        departureDate: departureDate,
        departureTime: departureTime,
        arrivalplace: arrivalplace,
        arrivalterminal: arrivalterminal,
        arrivalDate: arrivalDate,
        arrivalTime: arrivalTime,
        currency: currency,
        totalamount: totalamount,
        Flight_Stoppage_arrival:this.Flight_Stoppage_arrival,
        Flight_Stoppage_departure :this.Flight_Stoppage_departure,
        ReturnArrivalplace: ReturnArrivalplace,
        ReturnArrivalterminal:ReturnArrivalterminal,
        ReturnArrivalat:ReturnArrivalat,
        ReturnArrivalDate:ReturnArrivalDate,
        ReturnArrivalTime:ReturnArrivalTime,
        ReturnDepartureFrom:ReturnDepartureFrom,
        ReturnDepartureTerminal:ReturnDepartureTerminal,
        ReturnDepartureAt:ReturnDepartureAt,
        ReturnDepartureDate:ReturnDepartureDate,
        ReturnDepartureTime:ReturnDepartureTime,
        ReturnFlight_Stop:this.ReturnFlight_Stop,
        Flightname:this.Flight_Name,
        FlightStop:this.Flight_Stop,
        Flight_Details_Stop:Flight_Details_Stop,
        Flight_Details_Segment: Flight_Details_Segment,
        Flight_Details_travelerPricings: Flight_Details_travelerPricings,
        Flight_Adult_Detail : AdultTotal,
        Flight_Child_Detail: ChildTotal,
        Flight_Infant_Detail:InfantTotal,
        Flight_Total_Price:price
      });
      resolve({ Flight_options: this.Flight_options, totalresult:totalresult});
    }
    
    reject({Flight_options:"No data As per search"});
 })
 return promise;
 }
}
