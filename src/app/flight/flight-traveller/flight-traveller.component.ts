import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-flight-traveller',
  templateUrl: './flight-traveller.component.html',
  styleUrls: ['./flight-traveller.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FlightTravellerComponent implements OnInit {
  selected = 'option2';
  step = 0;
  show_insurence = false;
  show_ride_input = false;
  moreOption;
  customCollapsedHeight: string = '48px';
  customExpandedHeight: string = '48px';
  
  constructor() { }

  ngOnInit() {
  }


  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  showInsurence(){
    this.show_insurence = !this.show_insurence;
  }

  showRideinput(){
    this.show_ride_input = !this.show_ride_input;
  }

  moreOptionFun(){
    this.moreOption = !this.moreOption;
  }

}
