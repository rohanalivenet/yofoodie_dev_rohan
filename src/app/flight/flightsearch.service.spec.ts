import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';

import { FlightsearchService } from './flightsearch.service';

describe('FlightsearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule,HttpClientModule], 
    providers: [FlightsearchService]
  }));

  it('should be created', () => {
    const service: FlightsearchService = TestBed.get(FlightsearchService);
    expect(service).toBeTruthy();
  });
});
