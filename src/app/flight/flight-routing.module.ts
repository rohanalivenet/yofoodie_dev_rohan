import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { FlightComponent } from "./flight/flight.component";
import { FlightHomeComponent } from "./flight-home/flight-home.component";
import { FlightSearchComponent } from "./flight-search/flight-search.component";
import { FlightTravellerComponent } from "./flight-traveller/flight-traveller.component";

const routes: Routes = [
  {
    path: "flight",
    component: FlightComponent,
    children: [
      {
        path: "home",
        component: FlightHomeComponent
      },
      {
        path: "search",
        component: FlightSearchComponent
      },
      {
        path: "traveller",
        component: FlightTravellerComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FlightRoutingModule {}
