/// <reference types="@types/googlemaps" />
import {
  Component,
  OnInit,
  ViewEncapsulation,
  NgModule,
  ViewChild,
  EventEmitter,
  Output,
  AfterViewInit,
  ElementRef,
  Input
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { PlatformLocation, DatePipe } from "@angular/common";
//import { StorageMap } from "@ngx-pwa/local-storage";

import { NgForm, FormControl } from "@angular/forms";
import { Observable, from } from "rxjs";
import {
  map,
  filter,
  debounceTime,
  tap,
  switchMap,
  startWith
} from "rxjs/operators";
import { FlightService } from "../flight.service";
import { CommonserviceService } from "../../commonservice.service";

import { Flight } from "../flight";
import { AppState } from "../../../app/app.state";
import { Store } from "@ngrx/store";

import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE
} from "@angular/material/core";
import {
  MatMomentDateModule,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS
} from "@angular/material-moment-adapter";
import * as _moment from "moment";
import { default as _rollupMoment, Moment } from "moment";
import { stringify } from "querystring";
import { error } from "protractor";

const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: "LL"
  },
  display: {
    dateInput: "LL",
    monthYearLabel: "MMM YYYY",
    dateA11yLabel: "LL",
    monthYearA11yLabel: "MMMM YYYY"
  }
};
@Component({
  selector: "app-flight-home-search",
  templateUrl: "./flight-home-search.component.html",
  styleUrls: ["./flight-home-search.component.scss"],
  providers: [
    DatePipe,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]
})
export class FlightHomeSearchComponent implements OnInit {
  @Input() adressType: string;
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  model: any = new Flight("", "", "", "", null, null, "", "");

  Home_Router: string;
  Flight_departure_date: any;
  Flight_return_date: any;
  Flight_Choose_Trip: string;
  Flight_Adult_Number: number;
  Flight_Child_Number: number;
  Flight_class: string;
  selected: string;
  FlightCitySearch: Array<string> = [];
  myControl = new FormControl();
  tymyControl = new FormControl();
  options: any[] = [];
  optionsTo: any[] = [];
  filteredOptions: Observable<string[]>;
  filteredOptionsTo: Observable<string[]>;

  isLoadingFrom = false;
  errorMsgFrom: string;
  isLoadingTo = false;
  errorMsgTo: string;

  Flag1 = true;
  Flag2 = false;
  Flag3 = false;

  show_travellarbox = false;
  show_travellarbox_oneway = false;

  incCountValAdult = 0;
  incCountValChil = 0;
  incMulticityForm: string;

  flightCount: Array<any> = [1, 2];
  flight_max: number = 5;
  flight5 = true;

  constructor(
    private flightService: FlightService,
    private platformLocation: PlatformLocation,
    private router: Router,
    private route: ActivatedRoute,
    //private storage: StorageMap,
    private store: Store<AppState>,
    private datepipe: DatePipe,
    private commonService: CommonserviceService
  ) {
    this.model.flightnum = "1";
  }

  // Multi Flight Add
  addFlight(num) {
    if (this.flightCount.length < this.flight_max) {
      this.flightCount.push(this.flightCount.length + 1);
      if (this.flightCount.length == this.flight_max) {
        this.flight5 = false;
      }
    }
  }

  // onItemChange(value){
  //   console.log(" Value is : ", value.value );
  // }
  removeFlight(num) {
    this.flightCount.pop();
    if (this.flightCount.length < this.flight_max) {
      this.flight5 = true;
    }
    this.model.flightnum = String(this.flightCount.length);
  }

  //Initialise Form
  ngOnInit() {
    if (this.route.snapshot.paramMap.get("Flight_trip") == null) {
      this.Flight_Choose_Trip = "1";
    } else {
      this.Flight_Choose_Trip = this.route.snapshot.paramMap.get("Flight_trip");
    }
    this.model.Flight_trip = this.Flight_Choose_Trip;

    if (this.route.snapshot.paramMap.get("Flight_Class") == null) {
      this.selected = "ECONOMY";
    } else {
      this.selected = this.route.snapshot.paramMap.get("Flight_Class");
    }
    this.model.Flight_Class = this.selected;

    if (this.route.snapshot.paramMap.get("Flight_From") != null) {
      this.model.Flight_From = this.route.snapshot.paramMap.get("Flight_From");
    }
    if (this.route.snapshot.paramMap.get("Flight_To") != null) {
      this.model.Flight_To = this.route.snapshot.paramMap.get("Flight_To");
    }
    if (this.route.snapshot.paramMap.get("Fligt_Adult") == null) {
      this.Flight_Adult_Number = 1;
    } else {
      this.Flight_Adult_Number = parseFloat(
        this.route.snapshot.paramMap.get("Fligt_Adult")
      );
    }
    this.model.Fligt_Adult = this.incCountValAdult = this.Flight_Adult_Number;

    if (this.route.snapshot.paramMap.get("Fligt_child") == null) {
      this.Flight_Child_Number = 0;
    } else {
      this.Flight_Child_Number = parseFloat(
        this.route.snapshot.paramMap.get("Fligt_child")
      );
    }

    this.model.Fligt_child = this.incCountValChil = this.Flight_Child_Number;

    if (this.route.snapshot.paramMap.get("Flight_Departure") == null) {
      this.Flight_departure_date = new Date();
    } else {
      this.Flight_departure_date = new Date(
        this.datepipe.transform(
          this.route.snapshot.paramMap.get("Flight_Departure")
        )
      );
    }

    this.model.Flight_Departure = this.Flight_departure_date;

    if (this.route.snapshot.paramMap.get("Flight_ReturnDate") == null) {
      this.Flight_return_date = new Date();
    } else {
      this.Flight_return_date = new Date(
        this.datepipe.transform(
          this.route.snapshot.paramMap.get("Flight_ReturnDate")
        )
      );
    }

    this.model.Flight_ReturnDate = this.Flight_return_date;

    this.filteredOptions = this.myControl.valueChanges.pipe(
      filter(Text => Text.length > 1),
      debounceTime(500),
      tap(() => {
        this.errorMsgFrom = "";
        this.isLoadingFrom = true;
      }),
      switchMap(value =>
        this.finalsearchdata(value).finally(() => {
          this.isLoadingFrom = false;
        })
      )
    );

    this.filteredOptionsTo = this.tymyControl.valueChanges.pipe(
      filter(Text => Text.length > 1),
      debounceTime(500),
      tap(() => {
        this.errorMsgTo = "";
        this.isLoadingTo = true;
      }),
      switchMap(value =>
        this.finalsearchdataTo(value).finally(() => {
          this.isLoadingTo = false;
        })
      )
    );
  }

  private async finalsearchdata(value: string) {
    this.options = [];
    await this.flightService
      .airporTcitySearch(value)
      .then(data => {
        for (var i = 0; i < data["airporTCitySearch"][0]["data"].length; i++) {
          this.options.push({
            iatacode: data["airporTCitySearch"][0]["data"][i]["iataCode"],
            cityname:
              data["airporTCitySearch"][0]["data"][i]["address"]["cityName"],
            countryName:
              data["airporTCitySearch"][0]["data"][i]["address"][
                "countryName"
              ] +
              "--" +
              data["airporTCitySearch"][0]["data"][i]["name"],
            value:
              data["airporTCitySearch"][0]["data"][i]["address"]["cityName"] +
              " (" +
              data["airporTCitySearch"][0]["data"][i]["iataCode"] +
              ")"
          });
        }
        this.errorMsgFrom = "";
        this.isLoadingFrom = false;
      })
      .catch(error => {
        this.options = [];
        const errordata =
          error["airporTCitySearch"]["error"]["errors"][0]["title"];
        this.options.push(errordata);
      });
    return this.options;
  }

  private async finalsearchdataTo(value: string) {
    this.optionsTo = [];
    await this.flightService
      .airporTcitySearch(value)
      .then(data => {
        for (var i = 0; i < data["airporTCitySearch"][0]["data"].length; i++) {
          this.optionsTo.push({
            iatacode: data["airporTCitySearch"][0]["data"][i]["iataCode"],
            cityname:
              data["airporTCitySearch"][0]["data"][i]["address"]["cityName"],
            countryName:
              data["airporTCitySearch"][0]["data"][i]["address"][
                "countryName"
              ] +
              "--" +
              data["airporTCitySearch"][0]["data"][i]["name"],
            value:
              data["airporTCitySearch"][0]["data"][i]["address"]["cityName"] +
              " (" +
              data["airporTCitySearch"][0]["data"][i]["iataCode"] +
              ")"
          });
        }
        this.errorMsgTo = "";
        this.isLoadingTo = false;
      })
      .catch(error => {
        this.optionsTo = [];
        const errordata =
          error["airporTCitySearch"]["error"]["errors"][0]["title"];
        this.optionsTo.push(errordata);
      });
    return this.optionsTo;
  }


  invokeEvent(place: Object) {
    this.setAddress.emit(place);
  }
  invokeEvent1(place1: Object) {
    this.setAddress.emit(place1);
  }

  public getCount() {
    this.incCountValAdult = this.flightService.count;
  }
  public incCount() {
    this.incCountValAdult = this.flightService.add(this.incCountValAdult);
  }
  public decCount() {
    this.incCountValAdult = this.flightService.minus(this.incCountValAdult);
  }

  public getCountchil() {
    this.incCountValChil = this.flightService.count;
  }
  public incCountchil() {
    this.incCountValChil = this.flightService.add(this.incCountValChil);
  }
  public decCountchil() {
    this.incCountValChil = this.flightService.minus(this.incCountValChil);
  }

  showOneway() {
    this.Flag1 = true;
    this.Flag2 = false;
    this.Flag3 = false;
  }
  showRoundway() {
    this.Flag1 = true;
    this.Flag2 = true;
    this.Flag3 = false;
  }
  showMulticity() {
    this.Flag1 = true;
    this.Flag2 = true;
    this.Flag3 = true;
  }

  //On Check Url
  checkUrl() {
    this.Home_Router = this.router.url;
    if (this.Home_Router == "/flight/home") {
      console.log("Home Page");
    } else {
      //location.reload();
    }
  }

  //Travaler box popup

  showTravellarboxoneway() {
    this.show_travellarbox_oneway = !this.show_travellarbox_oneway;
  }

  showTravellarbox() {
    this.show_travellarbox = !this.show_travellarbox;
  }
  hideTravellarbox() {
    this.show_travellarbox = false;
  }

  //Form Onsubmit
  onSubmit(e) {
    // type MyArrayType = Array<{
    //   Flight_trip: string;
    //   Flight_Class: string;
    //   Flight_From: string;
    //   Flight_To: string;
    //   Fligt_Adult: number;
    //   Fligt_child: number;
    //   Flight_Departure: string;
    //   Flight_ReturnDate: string;
    // }>;
    // const arr: MyArrayType = [
    //   {
    //     Flight_trip: this.model.Flight_trip,
    //     Flight_Class: this.model.Flight_Class,
    //     Flight_From: this.Flight_From[1],
    //     Flight_To:  this.Flight_To[1],
    //     Fligt_Adult: this.model.Fligt_Adult,
    //     Fligt_child: this.model.Fligt_child,
    //     Flight_Departure: detparture_date,
    //     Flight_ReturnDate: return_date
    //   }
    // ];
    this.router.navigate([
      "/flight/search",
      {
        Flight_trip: this.model.Flight_trip,
        Flight_Class: this.model.Flight_Class,
        Flight_From: this.model.Flight_From,
        Flight_To: this.model.Flight_To,
        Fligt_Adult: this.incCountValAdult,
        Fligt_child: this.incCountValChil,
        Flight_Departure: this.model.Flight_Departure,
        Flight_ReturnDate: this.model.Flight_ReturnDate
      }
    ]);
    this.store.dispatch({
      type: "Add_Flight",
      payload: <Flight>{
        Flight_trip: this.model.Flight_trip,
        Flight_Class: this.model.Flight_Class,
        Flight_From: this.model.Flight_From,
        Flight_To: this.model.Flight_To,
        Fligt_Adult: this.incCountValAdult,
        Fligt_child: this.incCountValChil,
        Flight_Departure: this.model.Flight_Departure,
        Flight_ReturnDate: this.model.Flight_ReturnDate
      }
    });
    //this.commonService.localstorage(arr,"FlightForm");
  }
}
