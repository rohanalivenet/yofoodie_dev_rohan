import { Component, OnInit, ViewEncapsulation, ViewChild } from "@angular/core";
import { SwiperConfigInterface } from "ngx-swiper-wrapper";
import { MatExpansionPanel } from "@angular/material";
import { ActivatedRoute } from "@angular/router";
import { PlatformLocation, DatePipe } from "@angular/common";
import { FlightService } from "../flight.service";
import { FlightsearchService } from "../flightsearch.service";

import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE
} from "@angular/material/core";
import {
  MatMomentDateModule,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS
} from "@angular/material-moment-adapter";
import { element, promise } from 'protractor';
import { async } from 'rxjs/internal/scheduler/async';

@Component({
  selector: "app-flight-search",
  templateUrl: "./flight-search.component.html",
  styleUrls: ["./flight-search.component.scss"],
  providers: [
    DatePipe,
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    }
  ],
  encapsulation: ViewEncapsulation.None
})
export class FlightSearchComponent implements OnInit {
  Flag1 = true;
  Flag2 = false;
  Flag3 = false;
  Flag4 = true;
  Flag5 = false;
  searchFilter;
  searchModify;
  @ViewChild("first", { static: false }) first: MatExpansionPanel;
  @ViewChild("scond", { static: false }) scond: MatExpansionPanel;
  @ViewChild("third", { static: false }) third: MatExpansionPanel;
  @ViewChild("fourth", { static: false }) fourth: MatExpansionPanel;
  @ViewChild("fifth", { static: false }) fifth: MatExpansionPanel;
  radius_go = false;
  radius_go2 = false;
  radius_go3 = false;
  radius_go4 = false;
  radius_go5 = false;
  details_tab_show : any;
  ionContantHeight = "0px";
  isLoadingSearch=false;
  // Material Expansion Panel Custom Height
  customCollapsedHeight: string = "40px";
  customExpandedHeight: string = "40px";

  show_travellarbox = false;
  show_travellarbox_oneway = false;
  Flight_Trip: string = "";
  Flight_Class: string = "";
  Flight_From: string = "";
  Flight_To: string = "";
  Fligt_Adult: string = "";
  Flight_Child: string = "";
  Flight_Departure: string = "";
  Flight_ReturnDate: string = "";
  Flight_Name:string="";
  Flight_Stop:string="";
  Flight_Currency:string="";
  Flight_from: Array<any> = [];
  Flight_to: Array<any> = [];
  Flight_options: any[] = [];
  carriers: Array<any> = [];

  date_rate: Array<any> = [];

  constructor(
    private route: ActivatedRoute,
    private datepipe: DatePipe,
    private flightService: FlightService,
    private flightsearchService :FlightsearchService
  ) {
    this.date_rate = [
      { date: "Sun, 15 May", rate: "$17.05" },
      { date: "Mon, 16 May", rate: "$20.07" },
      { date: "Tue, 17 May", rate: "$22.02" },
      { date: "Wed, 18 May", rate: "$25.07" },
      { date: "Thu, 19 May", rate: "$27.00" },
      { date: "Fry, 20 May", rate: "$26.07" },
      { date: "Sat, 21 May", rate: "$24.00" },
      { date: "Sun, 22 May", rate: "$22.07" },
      { date: "Mon, 23 May", rate: "$20.01" },
      { date: "Tue, 24 May", rate: "$21.07" }
    ];
  }
  F_From1;
  F_to1;


  ngOnInit() {
    this.Flight_Trip = this.route.snapshot.paramMap.get("Flight_trip");
    this.Flight_Class = this.route.snapshot.paramMap.get("Flight_Class");
    this.Flight_From = this.route.snapshot.paramMap.get("Flight_From");

    this.F_From1 = this.Flight_From.split(/[()]/);

    this.Flight_To = this.route.snapshot.paramMap.get("Flight_To");

    this.F_to1 = this.Flight_To.split(/[()]/);

    this.Fligt_Adult = this.route.snapshot.paramMap.get("Fligt_Adult");
    this.Flight_Child = this.route.snapshot.paramMap.get("Fligt_child");
    this.Flight_Departure = this.route.snapshot.paramMap.get(
      "Flight_Departure"
    );
    this.Flight_ReturnDate = this.route.snapshot.paramMap.get(
      "Flight_ReturnDate"
    );

    this.Flight_from = this.Flight_From.split(/[()]/);
    this.Flight_to = this.Flight_To.split(/[()]/);
    let detparture_date = this.datepipe.transform(
      this.Flight_Departure,
      "yyyy-MM-dd"
    );
    let return_date = this.datepipe.transform(
      this.Flight_ReturnDate,
      "yyyy-MM-dd"
    );
    this.Flight_Stop="false";
    this.Flight_Currency="INR"


  this.isLoadingSearch = true;
   this.flightService.flightSearch(
        this.Flight_Trip,
        this.Flight_from[1],
        this.Flight_to[1],
        detparture_date,
        return_date,
        this.Fligt_Adult,
        this.Flight_Child,
        this.Flight_Class,
        this.Flight_Stop,
        this.Flight_Currency
      )
      .then(data => {
       this.flightsearchService.searchdatacal(data).then(data=>{
        this.Flight_options.push(data);
        this.isLoadingSearch=false;
        console.log(data);
       });
      })
      .catch(error => {
        var errorcode = error["flightsearch"]["error"]["errors"][0]["title"];
        this.Flight_options.push(errorcode);
      });

    console.log(
      "Flight Trip:" +
        this.Flight_Trip +
        "," +
        "Flight Class:" +
        this.Flight_Class +
        "," +
        "Flight_From:" +
        this.Flight_from[1] +
        "," +
        "Flight_To:" +
        this.Flight_to[1] +
        "," +
        "Flight_Adult:" +
        this.Fligt_Adult +
        "," +
        "Flight_Child:" +
        this.Flight_Child +
        "," +
        "Flight_Depart:" +
        detparture_date +
        "," +
        "Flight_Return:" +
        return_date + ","+
        "Flight Name"+ this.Flight_Name
    );
  }

  modifyFun() {
    this.searchModify = !this.searchModify;
  }
  refineFun() {
    this.searchFilter = !this.searchFilter;
  }

  showTravellarboxoneway() {
    this.show_travellarbox_oneway = !this.show_travellarbox_oneway;
  }

  showTravellarbox() {
    this.show_travellarbox = !this.show_travellarbox;
  }
  hideTravellarbox() {
    this.show_travellarbox = false;
  }
  // Radio input tab function
  // --Different form will show by clicking on different Radio input

  showOneway() {
    this.Flag1 = true;
    this.Flag2 = false;
    this.Flag3 = false;
  }
  showRoundway() {
    this.Flag1 = true;
    this.Flag2 = true;
    this.Flag3 = false;
  }
  showMulticity() {
    this.Flag1 = true;
    this.Flag2 = true;
    this.Flag3 = true;
  }

  // Dextop filter searching Ui functions

  showRadius() {
    this.radius_go = !this.radius_go;
    this.scond.close();
    this.third.close();
    this.fourth.close();
    this.fifth.close();
    this.radius_go2 = false;
    this.radius_go3 = false;
    this.radius_go4 = false;
    this.radius_go5 = false;
  }
  showRadius2() {
    this.radius_go2 = !this.radius_go2;
    this.first.close();
    this.third.close();
    this.fourth.close();
    this.fifth.close();
    this.radius_go = false;
    this.radius_go3 = false;
    this.radius_go4 = false;
    this.radius_go5 = false;
  }
  showRadius3() {
    this.radius_go3 = !this.radius_go3;
    this.first.close();
    this.scond.close();
    this.fourth.close();
    this.fifth.close();
    this.radius_go = false;
    this.radius_go2 = false;
    this.radius_go4 = false;
    this.radius_go5 = false;
  }
  showRadius4() {
    this.radius_go4 = !this.radius_go4;
    this.first.close();
    this.scond.close();
    this.third.close();
    this.fifth.close();
    this.radius_go = false;
    this.radius_go2 = false;
    this.radius_go3 = false;
    this.radius_go5 = false;
  }
  showRadius5() {
    this.radius_go5 = !this.radius_go5;
    this.first.close();
    this.scond.close();
    this.third.close();
    this.fourth.close();
    this.radius_go = false;
    this.radius_go2 = false;
    this.radius_go3 = false;
    this.radius_go4 = false;
  }
  showdetailsTab(i) {

    this.details_tab_show = i;
  }
  hidedetailsTab() {
    this.details_tab_show = -1;
  }

  public config: SwiperConfigInterface = {
    a11y: true,
    direction: "horizontal",
    slidesPerView: 8,
    keyboard: true,
    mousewheel: true,
    speed: 700,
    scrollbar: false,
    pagination: false,
    spaceBetween: 0,
    navigation: {
      nextEl: ".swiper-button-next1",
      prevEl: ".swiper-button-prev1"
    },
    breakpoints: {
      // when window width is >= 550px
      575: {
        slidesPerView: 4,
        spaceBetween: 0
      },
      // when window width is >= 768px
      991: {
        slidesPerView: 5,
        spaceBetween: 0
      },
      // when window width is >= 999px
      999: {
        slidesPerView: 7,
        spaceBetween: 0
      }
    }
  };
}
