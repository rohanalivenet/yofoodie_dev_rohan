import { Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FlightComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
