import { Injectable, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { GeneratetokenService } from "../generatetoken.service";
import { error, promise } from "protractor";

@Injectable({
  providedIn: "root"
})
export class FlightService {
  base_url: string = "https://test.api.amadeus.com";
  token_type: string;
  url: string;
  access_token: string;
  airporTCitySearch: Array<string> = [];
  flightsearch: Array<string> = [];
  result: string;
  public finalairportCitySearch: Array<any> = [];
  constructor(
    private genrateToken: GeneratetokenService,
    private http: HttpClient
  ) {
    // this.genrateToken.sendTokenRequest().then((res)=>{
    //   this.token_type = (res['readToken'][0]['token_type']);
    //   this.access_token = (res['readToken'][0]['access_token']);
    //  console.log(this.token_type+','+this.access_token);
    // });
  }

  public count = 0;

  add(data) {
    return data + 1;
  }
  minus(data) {
    if (data < 1) {
      return 0;
    } else {
      return data - 1;
    }
  }

  airporTcitySearch(filtervalue: string) {
    this.airporTCitySearch = [];
    let promise = new Promise((resolve, reject) => {
      let url: string =
        this.base_url +
        "/v1/reference-data/locations?subType=AIRPORT,CITY&keyword=" +
        filtervalue +
        "&view=LIGHT&page[limit]=5";
      this.genrateToken.sendTokenRequest().then(res => {
        this.token_type = res["readToken"][0]["token_type"];
        this.access_token = res["readToken"][0]["access_token"];

        const headers = new HttpHeaders().set(
          "Authorization",
          this.token_type + " " + this.access_token
        );

        this.http
          .get<any>(url, { headers: headers })
          .toPromise()
          .then(
            res => {
              this.airporTCitySearch.push(res);
              resolve({ airporTCitySearch: this.airporTCitySearch });
            },
            error => {
              reject({ airporTCitySearch: error });
            }
          );
      });
    });
    return promise;
  }

  flightSearch(
    Flight_Trip,
    Flight_From,
    Flight_To,
    Flight_Departure,
    Flight_ReturnDate,
    Fligt_Adult,
    Fligt_child,
    Flight_class,
    Flight_Stop,
    Flight_currency
  ) {
    this.flightsearch = [];
    let promise = new Promise((resolve, reject) => {
      if (Flight_Trip == 2) {
        this.url =
          this.base_url +
          "/v2/shopping/flight-offers?originLocationCode=" +
          Flight_From +
          "&destinationLocationCode=" +
          Flight_To +
          "&departureDate=" +
          Flight_Departure +
          "&returnDate=" +
          Flight_ReturnDate +
          "&adults=" +
          Fligt_Adult +
          "&children=" +
          Fligt_child +
          "&travelClass=" +
          Flight_class +
          "&nonStop="+Flight_Stop+
          "&currencyCode="+Flight_currency+"&max=250";
      } else {
        this.url =
          this.base_url +
          "/v2/shopping/flight-offers?originLocationCode=" +
          Flight_From +
          "&destinationLocationCode=" +
          Flight_To +
          "&departureDate=" +
          Flight_Departure +
          "&adults=" +
          Fligt_Adult +
          "&children=" +
          Fligt_child +
          "&travelClass=" +
          Flight_class +
          "&nonStop="+Flight_Stop +
          "&currencyCode="+Flight_currency+"&max=250";
      }
      const headers = new HttpHeaders().set(
        "Authorization",
        this.token_type + " " + this.access_token
      );
      this.http
        .get<any>(this.url, { headers: headers })
        .toPromise()
        .then(
          res => {
            this.flightsearch.push(res);
            resolve({ flightsearch: this.flightsearch });
          },
          error => {
            reject({ flightsearch: error });
          }
        );
    });
    return promise;
  }
}
