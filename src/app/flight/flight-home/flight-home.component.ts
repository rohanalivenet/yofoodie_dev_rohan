import {
  Component,
  OnInit,
  ViewEncapsulation,
  NgModule,
  ViewChild,
  EventEmitter,
  Output,
  AfterViewInit,
  ElementRef,
  Input
} from "@angular/core";

import { SwiperConfigInterface } from "ngx-swiper-wrapper";
import { EventEmitterService } from "../../event-emitter.service";
import { NgForm, FormControl } from "@angular/forms";
import { FlightService } from "../flight.service";
import { StorageMap } from "@ngx-pwa/local-storage";

import { Observable } from "rxjs";
import { map, startWith } from "rxjs/operators";
import {CommonserviceService } from "../../commonservice.service";


@Component({
  selector: "app-flight-home",
  templateUrl: "./flight-home.component.html",
  styleUrls: ["./flight-home.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class FlightHomeComponent implements OnInit {
  
  constructor(
    private eventEmitterService: EventEmitterService,
    private storage: StorageMap,
    private commonService:CommonserviceService
  ) {}
Flight_From:string;
Flight_To:string;
Flight_Trip:string;
Flight_Departure:string;

  ngOnInit() {
   this.commonService.readlocalstorage("FlightForm").then((res)=>{
    if(typeof res['readlocalstoragedata'][0]=='undefined'){
      console.log("No data in local storage");
    }else{
    const data=JSON.parse(res['readlocalstoragedata'][0]);
    if(data[0]['Flight_trip']=='2'){
      this.Flight_Trip = "Round Trip";  
    }else if(data[0]['Flight_trip']=='3'){
      this.Flight_Trip = "Multi-City";  
    }else{
      this.Flight_Trip ="One Way";
    }
    this.Flight_From = data[0]['Flight_From'];
    this.Flight_To = data[0]['Flight_To'];
    this.Flight_Departure=data[0]['Flight_Departure'];
    }
    
   });
  
  }

  

  public handleAddressChange(address: any) {
    // Do some stuff
}
  public config: SwiperConfigInterface = {
    a11y: true,
    direction: "horizontal",
    slidesPerView: 4,
    keyboard: false,
    mousewheel: true,
    speed: 700,
    scrollbar: false,
    preventClicks: false,
    navigation: {
      nextEl: ".swiper-button-next1",
      prevEl: ".swiper-button-prev1"
    },
    pagination: {
      el: ".swiper-pagination1",
      type: "bullets",
      clickable: true
    },
    spaceBetween: 25,
    breakpoints: {
      // when window width is >= 768px
      767: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      // when window width is >= 999px
      999: {
        slidesPerView: 3,
        spaceBetween: 30
      }
    }
  };

  // Activity slider
  public config2: SwiperConfigInterface = {
    a11y: true,
    direction: "horizontal",
    slidesPerView: 3,
    keyboard: false,
    mousewheel: true,
    speed: 700,
    scrollbar: false,
    preventClicks: false,
    navigation: {
      nextEl: ".swiper-button-next-activity",
      prevEl: ".swiper-button-prev-activity"
    },
    pagination: {
      el: ".swiper-pagination2",
      type: "bullets",
      clickable: true
    },
    spaceBetween: 25,
    breakpoints: {
      // when window width is >= 768px
      767: {
        slidesPerView: 1,
        spaceBetween: 0
      },
      // when window width is >= 999px
      999: {
        slidesPerView: 2,
        spaceBetween: 30
      }
    }
  };

  firstComponentFunction() {
    this.eventEmitterService.onFirstComponentButtonClick();
  }

  
}
