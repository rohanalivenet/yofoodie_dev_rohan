import { Injectable } from '@angular/core';
import { StorageMap } from "@ngx-pwa/local-storage";
import { filter,map } from 'rxjs/operators';
import { error } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class CommonserviceService {

  datarray:string;
  storagename:string;
  data:string;
  readlocalstoragedata:Array<any>=[];
  

  constructor(private storage: StorageMap,) { }

  localstorage(dataarray,storagename){
    let data: string = JSON.stringify(dataarray);
    this.storage.set(storagename, data).subscribe({
      next: x => {
        console.log("Observer got a next value: " + x);
      },
      error: error => {
        console.log("Observer got an error: " + error);
      },
      complete: () => {
        console.log("Observer got a complete notification");
      }
    });
  }
  readlocalstorage(storagename){
    let promise= new Promise((resolve,reject)=>{
      this.storage.get(storagename).toPromise().then((res)=>{
        this.readlocalstoragedata.push(res);
        resolve({readlocalstoragedata:this.readlocalstoragedata});
      },(error)=>{
        reject({readlocalstoragedata:error});
      })
    })
    return promise;
  }
}
