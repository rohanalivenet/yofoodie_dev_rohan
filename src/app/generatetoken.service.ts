import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class GeneratetokenService {
  base_url: string = "https://test.api.amadeus.com";

  url: string = this.base_url + "/v1/security/oauth2/token";
  readToken: Array<any> = [];

  constructor(private http: HttpClient) {}

  sendTokenRequest() {
    const headers = new HttpHeaders().set(
      "content-type",
      "application/x-www-form-urlencoded"
    );

    const body =
      "grant_type=client_credentials" +
      "&client_id=dGHB4qJCbwJVym1AqopkCbfciVnY1wQ7" +
      "&client_secret=EC0gV5oLtJxAaAQl";

    let promise = new Promise((resolve, reject) => {
      this.http
        .post(this.url, body, { headers: headers })
        .toPromise()
        .then(
          res => {
            this.readToken.push(res);
            resolve({ readToken: this.readToken });
          },
          error => {
            reject({ readToken: error });
          }
        );
    });

    return promise;
  }
}

//put catch
