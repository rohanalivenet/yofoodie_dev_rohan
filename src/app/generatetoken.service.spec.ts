import { TestBed } from '@angular/core/testing';
import {HttpClientModule} from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GeneratetokenService } from './generatetoken.service';
import { Observable,of } from 'rxjs'; // Add import

describe('GeneratetokenService', () => {
  let tokenservice:GeneratetokenService;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule,HttpClientModule], 
    providers: [GeneratetokenService]
  }));

  it('should be created', () => {
    const service: GeneratetokenService = TestBed.get(GeneratetokenService);
    expect(service).toBeTruthy();
  });

  describe('sendTokenRequest',()=>{
    it('Should return a details',()=>{
      const userResponse = [
        {
          type: "amadeusOAuth2Token",
            username: "hello@uiigo.com",
            application_name: "test app",
            client_id: "dGHB4qJCbwJVym1AqopkCbfciVnY1wQ7",
            token_type: "Bearer",
            access_token: "eiT6DwA5Ta38lvy1YOt5DY9GdkzT",
            expires_in: 1799,
            state: "approved",
            scope: ""

        }
      ];
      let response;
      spyOn(tokenservice,'sendTokenRequest').and.returnValue((Promise.resolve(userResponse)));
      tokenservice.sendTokenRequest().then(data=>{
        response = data;
      });
      expect(response).toEqual(userResponse);
    })
  })
});
