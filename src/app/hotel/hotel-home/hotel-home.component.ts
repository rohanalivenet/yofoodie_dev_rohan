import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
@Component({
  selector: 'app-hotel-home',
  templateUrl: './hotel-home.component.html',
  styleUrls: ['./hotel-home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HotelHomeComponent implements OnInit {
  selected:any = "Economy";
  constructor() { }

  ngOnInit() {
  }

  public config: SwiperConfigInterface = {
    a11y: true,
    direction: 'horizontal',
    slidesPerView: 4,
    keyboard: false,
    mousewheel: true,
    speed: 700,
    scrollbar: false,
    preventClicks: false,
    navigation: {
      nextEl: '.swiper-button-next1',
      prevEl: '.swiper-button-prev1',
    },
    pagination: {
      el: '.swiper-pagination1',
      type: 'bullets',
      clickable: true
    },  
    spaceBetween: 25,
    breakpoints: {
      // when window width is >= 768px
       767: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      // when window width is >= 999px
      999: {
        slidesPerView: 3,
        spaceBetween: 30
      }
    }
  };
  
  // Activity slider
  public config2: SwiperConfigInterface = {
    a11y: true,
    direction: 'horizontal',
    slidesPerView: 3,
    keyboard: false,
    mousewheel: true,
    speed: 700,
    scrollbar: false,
    preventClicks: false,
    navigation: {
      nextEl: '.swiper-button-next-activity',
      prevEl: '.swiper-button-prev-activity',
    },
    pagination: {
      el: '.swiper-pagination2',
      type: 'bullets',
      clickable: true
    },  
    spaceBetween: 25,
    breakpoints: {
      // when window width is >= 768px
       767: {
        slidesPerView: 1,
        spaceBetween: 0
      },
      // when window width is >= 999px
      999: {
        slidesPerView: 2,
        spaceBetween: 30
      }
    }
  };


}
