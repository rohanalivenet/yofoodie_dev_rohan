import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import {MatExpansionPanel} from '@angular/material';
@Component({
  selector: 'app-hotel-search',
  templateUrl: './hotel-search.component.html',
  styleUrls: ['./hotel-search.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HotelSearchComponent implements OnInit {

  searchOnMap = false;

  @ViewChild('first',{static: false}) first: MatExpansionPanel;
  @ViewChild('scond',{static: false}) scond: MatExpansionPanel;
  @ViewChild('third',{static: false}) third: MatExpansionPanel;
  @ViewChild('fourth',{static: false}) fourth: MatExpansionPanel;
  radius_go = false;
  radius_go2 = false;
  radius_go3 = false;
  radius_go4 = false;
  selected = '1';
  step = 0;

  // Material Expansion Panel Custom Height
  customCollapsedHeight: string = '40px';
  customExpandedHeight: string = '40px';
  searchFilter;
  searchModify;

  constructor() { }

 ngOnInit() {
  }
  
  setStep(index: number) {
    this.step = index;
  }

  


  searchMap(){
    this.searchOnMap = !this.searchOnMap;
    this.searchFilter = false;
    this.searchModify = false;
  }

 

modifyFun(){
  this.searchModify = !this.searchModify;

}
refineFun(){
  this.searchFilter = !this.searchFilter;
}

showRadius(){
  this.radius_go=!this.radius_go;
  this.scond.close();
  this.third.close();
  this.fourth.close();
  this.radius_go2 = false;
  this.radius_go3 = false;
  this.radius_go4 = false;
}
showRadius2(){
  this.radius_go2=!this.radius_go2;
  this.first.close();
  this.third.close();
  this.fourth.close();
  this.radius_go = false;
  this.radius_go3 = false;
  this.radius_go4 = false;
}
showRadius3(){
  this.radius_go3=!this.radius_go3;
  this.first.close();
  this.scond.close()
  this.fourth.close();
  this.radius_go = false;
  this.radius_go2 = false;
  this.radius_go4 = false;
}
showRadius4(){
  this.radius_go4=!this.radius_go4;
  this.first.close();
  this.scond.close();
  this.third.close();
  this.radius_go = false;
  this.radius_go2 = false;
  this.radius_go3 = false;
}


}
