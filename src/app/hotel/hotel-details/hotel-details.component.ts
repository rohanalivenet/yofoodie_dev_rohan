import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
@Component({
  selector: 'app-hotel-details',
  templateUrl: './hotel-details.component.html',
  styleUrls: ['./hotel-details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HotelDetailsComponent implements OnInit {
 step = 0;
 selectedOption = '1';
 images: Array<any> = [];
  constructor() { 

    this.images = [
      "slide1",
      "slide2",
      "slide3",
      "slide1",
      "slide2",
      "slide3",
      "slide1",
      "slide2",
      "slide3",
    ]

  }

  ngOnInit() {
  }
  gotoTop(){ }
  gotoDetails(){ }
  setStep(index: number) {
    this.step = index;
  }


  // Swiper slider
public config2: SwiperConfigInterface = {
  a11y: true,
  direction: 'horizontal',
  slidesPerView: 1,
  keyboard: false,
  mousewheel: false,
  speed: 700,
  scrollbar: false,
  navigation: false,  
  pagination: {
    el: '.slide-show-thubnail ul',
    renderBullet:  (index, className)=> {
      return '<li class="' + className + '"><img src="assets/img/room/'+(this.images[index])+'.jpg"></li>';
    },
    clickable: true
  },  
  preventClicks: true,
  preventClicksPropagation: false,
  spaceBetween: 0,
};




// Swiper slider
public config: SwiperConfigInterface = {
  a11y: true,
  direction: 'horizontal',
  slidesPerView: 3,
  keyboard: true,
  mousewheel: true,
  speed: 700,
  scrollbar: false,
  navigation: {
    nextEl: '.swiper-button-next1',
    prevEl: '.swiper-button-prev1',
  },
  pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
    clickable: true
  },  
  preventClicks: true,
  preventClicksPropagation: false,
  spaceBetween: 25,
  breakpoints: {
    // when window width is >= 575px
    //  575: {
    //   slidesPerView: 1,
    //   spaceBetween: 5,
    // },
    // when window width is >= 768px
     767: {
      slidesPerView: 1,
      spaceBetween: 5,
    },
    // when window width is >= 999px
    999: {
      slidesPerView: 2,
      spaceBetween: 15
    }
  } 
};

}
