import { Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-guest-details',
  templateUrl: './guest-details.component.html',
  styleUrls: ['./guest-details.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GuestDetailsComponent implements OnInit {
 // Material Expansion Panel Custom Height
 customCollapsedHeight: string = '45px';
 customExpandedHeight: string = '45px';
 moreOption;
 step = 0;
 selectedOption = '1';
  constructor() { }

  ngOnInit() {
  }
  setStep(index: number) {
    this.step = index;
  }
  moreOptionFun(){
    this.moreOption = !this.moreOption;
  }

}
