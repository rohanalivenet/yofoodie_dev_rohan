import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HotelComponent } from './hotel/hotel.component';
import { HotelHomeComponent } from './hotel-home/hotel-home.component';
import { HotelSearchComponent } from './hotel-search/hotel-search.component';
import { HotelDetailsComponent } from './hotel-details/hotel-details.component';
import { GuestDetailsComponent } from './guest-details/guest-details.component';

const routes: Routes = [
    {
        path: 'hotel',
        component: HotelComponent,
        children: [
            {
                path: 'home',
                component: HotelHomeComponent
            },{
                path: 'search',
                component:HotelSearchComponent
            },{
                path: 'details',
                component:HotelDetailsComponent
            },{
                path: 'guests',
                component:GuestDetailsComponent
            }
        ]
    }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HotelRoutingModule { }
