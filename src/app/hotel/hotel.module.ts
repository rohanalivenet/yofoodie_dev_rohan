import { BrowserModule } from '@angular/platform-browser';
import { AngularMaterialModule } from '../material.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { HotelRoutingModule } from './hotel-routing.module';
import { FormsModule } from '@angular/forms';


import { HotelComponent } from './hotel/hotel.component';
import { HotelHomeComponent } from './hotel-home/hotel-home.component';
import { HotelSearchComponent } from './hotel-search/hotel-search.component';


import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { HotelDetailsComponent } from './hotel-details/hotel-details.component';
import { GuestDetailsComponent } from './guest-details/guest-details.component';


const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};


@NgModule({
  declarations: [HotelComponent, HotelSearchComponent, HotelHomeComponent, HotelDetailsComponent, GuestDetailsComponent],
  imports: [
    BrowserModule,
    AngularMaterialModule,
    HotelRoutingModule,
    FormsModule, 
    SwiperModule,
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ]
})
export class HotelModule { }
