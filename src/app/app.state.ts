// app.state.ts

import { Flight } from './flight/flight';

export interface AppState {
  readonly blockchain: Flight[];
}