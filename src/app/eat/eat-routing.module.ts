import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EatComponent } from './eat/eat.component';
import { EatHomeComponent } from './eat-home/eat-home.component';
import { EatSearchComponent } from './eat-search/eat-search.component';

const routes: Routes = [
    {
        path: 'eat',
        component: EatComponent,
        children: [
            {
                path: 'home',
                component: EatHomeComponent
            },{
                path: 'search',
                component:EatSearchComponent
            }
        ]
    }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EatRoutingModule { }
