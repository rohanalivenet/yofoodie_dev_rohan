import { BrowserModule } from '@angular/platform-browser';
import { AngularMaterialModule } from '../material.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';


import { CommonModule } from '@angular/common';

import { EatRoutingModule } from './eat-routing.module';

import { EatSearchComponent } from './eat-search/eat-search.component';
import { EatHomeComponent } from './eat-home/eat-home.component';
import { EatComponent } from './eat/eat.component';




@NgModule({
  declarations: [EatSearchComponent, EatHomeComponent, EatComponent],
  imports: [
    BrowserModule,
    AngularMaterialModule,
    CommonModule,
    EatRoutingModule
  ]
})
export class EatModule { }
