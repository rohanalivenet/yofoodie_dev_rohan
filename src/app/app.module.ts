import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from '@angular/common/http';
import { AngularMaterialModule } from "./material.module";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { AppComponent } from "./app.component";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { EventEmitterService } from "../app/event-emitter.service";

import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";

import { HeaderComponent } from "./header/header.component";

import { FooterComponent } from "./footer/footer.component";
import { AppRoutingModule } from "./app-routing.module";
import { FlightModule } from "./flight/flight.module";
import { HotelModule } from "./hotel/hotel.module";
import { EatModule } from "./eat/eat.module";
//import { StorageModule } from '@ngx-pwa/local-storage';
import { StoreModule } from '@ngrx/store';
import { addCoinReducer } from './reducers/app.reducer';


@NgModule({
  declarations: [AppComponent, HeaderComponent, FooterComponent],
  imports: [
    GooglePlaceModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FlightModule,
    HotelModule,
    EatModule,
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production
    }),
    StoreModule.forRoot({blockchain: addCoinReducer})
    //StorageModule.forRoot({ IDBNoWrap: true })
  ],
  providers: [EventEmitterService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
