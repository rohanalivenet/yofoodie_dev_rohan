import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { FlightComponent } from './flight/flight/flight.component';
import { HotelComponent } from './hotel/hotel/hotel.component';
import { EatComponent } from './eat/eat/eat.component';


const routes: Routes = [

  { path: '', redirectTo: '/flight/home', pathMatch: 'full' },
  { path: 'flight/flight/home', component: FlightComponent },
  { path: 'eat/eat/home', component:EatComponent},  
  { path: 'hotel/hotel/home', component: HotelComponent }, 
  {path: 'home',component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
