import { Component, OnInit } from '@angular/core';
import { EventEmitterService } from '../event-emitter.service';  

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  mobile_navbar:any = false;
  navLink:Array<any> = [];
  constructor(private eventEmitterService: EventEmitterService) {

    this.navLink = [
      {name:'home', routerlinks:'/flight/home', icon:'home'},
      {name:'hotel', routerlinks:'/hotel/home', icon:'home'},
    ]

    


   }

  ngOnInit() {    
    if (this.eventEmitterService.subsVar==undefined) {    
      this.eventEmitterService.subsVar = this.eventEmitterService.    
      invokeFirstComponentFunction.subscribe((name:string) => {    
        this.firstFunction();    
      });    
    }    
  }    
    
  firstFunction() {    
    this.mobile_navbar = !this.mobile_navbar
    // alert( 'Hello ' + '\nWelcome to C# Corner \nFunction in First Component');    
  } 

  


}
